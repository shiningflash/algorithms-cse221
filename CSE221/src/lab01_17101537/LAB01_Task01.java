package lab01_17101537;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Sep 22, 2018
 **/

import java.util.EmptyStackException;
import java.util.Stack;
import java.util.*;

public class LAB01_Task01 {
	static void stack_push(Stack stack, Integer n) {
		System.out.println("Pushing " + n + "....");
		stack.push(n);
		System.out.println("Printing top:  " + stack.peek());
	}
	static void stack_pop(Stack stack) {
		try {
			stack.pop();
			System.out.println("Popping....");
			System.out.println("Printing top:  " + stack.peek());
		} catch(EmptyStackException e) {
			System.out.println("empty stack....");
		}
	}
	public static void main(String[] args) {
		Stack<Integer> stack = new<Integer> Stack();
		stack_push(stack, 10);
		stack_push(stack, 5);
		stack_push(stack, 6);
		stack_pop(stack);
		stack_push(stack, 9);
		stack_push(stack, 3);
		stack_push(stack, 2);
		stack_pop(stack);
		stack_pop(stack);
		stack_pop(stack);
		stack_pop(stack);
		stack_pop(stack);
		stack_pop(stack);
	}
}
