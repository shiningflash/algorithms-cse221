Undirected Graph…..
  Adjacency Matrix :-
----------------
    0 1 2 3 4 5 
   ------------
0 | 0 1 0 1 0 1 
1 | 1 0 0 0 1 1 
2 | 0 0 0 0 1 0 
3 | 1 0 0 0 1 0 
4 | 0 1 1 1 0 0 
5 | 1 1 0 0 0 0 

Directed Graph…..
  Adjacency Matrix :-
---------------
    0 1 2 3 4 5 
   ------------
0 | 0 1 0 1 0 1 
1 | 0 0 0 0 1 1 
2 | 0 0 0 0 1 0 
3 | 0 0 0 0 1 0 
4 | 0 0 0 0 0 0 
5 | 0 0 0 0 0 0 

Adjacency List: -
--------------
0 --> [3, 1, 5]
1 --> [0, 4, 5]
2 --> [4]
3 --> [0, 4]
4 --> [1, 2, 3]
5 --> [0, 1]

Out degree: -
-----------
0 --> 3
1 --> 3
2 --> 1
3 --> 2
4 --> 3
5 --> 2

Out/IN degree: -
-------------
0 --> 3/0
1 --> 2/1
2 --> 1/0
3 --> 1/1
4 --> 0/3
5 --> 0/2

