package lab01_17101537;

/**
* @author  Amirul Islam Al Mamun
* @id      17101537
* @date    Sep 22, 2018
* 
* @task done here,
* read a input file first "input.txt"
* Graph G(V, E) --> Undirected Graph Adjacency Matrix
* Graph G(V, E) --> Directed Graph Adjacency Matrix
* Graph G(V, E) --> Adjacency List
* Graph G(V, E) --> Vertex wise out Degree for Undirected Graph
* Graph G(V, E) --> Vertex wise Out/IN degree for Directed Graph
**/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

public class LAB01_Task03 {
	public static void main (String[] args) {
		ArrayList <String> inputLine = new ArrayList<String>();
		try (BufferedReader r = new BufferedReader(new FileReader("/home/mamun/workspace/CSE220/CSE221/src/lab01_17101537/input.txt"))) {
			// my Linux PC file location
			String line;
			while ((line = r.readLine()) != null) {
				inputLine.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int numberOfVertex = Integer.parseInt(inputLine.get(0));
		
		LinkedList<Integer> adjList[] = new LinkedList[numberOfVertex];
		for (int i =0; i < numberOfVertex; i++)
			adjList[i] = new LinkedList<Integer>();
		
		int matrixForUndirected[][] = new int[numberOfVertex][numberOfVertex];
		int matrixForDirected[][] = new int[numberOfVertex][numberOfVertex];
		
		int totalDegree[] = new int[numberOfVertex];
		int inDegree[] = new int[numberOfVertex];
		int outDegree[] = new int[numberOfVertex];
		
		for (int i = 1; i < inputLine.size(); i++) {
			String temp[] = inputLine.get(i).split(" ", 2);
			int theVertex = Integer.parseInt(temp[0]);
			int connectedVertex = Integer.parseInt(temp[1]);
			
			adjList[theVertex].add(connectedVertex);
			adjList[connectedVertex].add(theVertex);
			
			matrixForUndirected[theVertex][connectedVertex] = 1;
			matrixForUndirected[connectedVertex][theVertex] = 1;
			
			matrixForDirected[theVertex][connectedVertex] = 1;
			
			totalDegree[theVertex]++;
			totalDegree[connectedVertex]++;
			outDegree[theVertex]++;
			inDegree[connectedVertex]++;
		}
		
		System.out.println("Undirected Graph…..\n  Adjacency Matrix :-\n----------------");
		System.out.print("    ");
		for (int i = 0; i < numberOfVertex; i++)
			System.out.print(i + " ");
		System.out.println("\n   ------------");
		for (int i = 0; i < numberOfVertex; i++) {
			System.out.print(i + " | ");
			for (int j = 0; j < numberOfVertex; j++)
				System.out.print(matrixForUndirected[i][j] + " ");
			System.out.println();
		}
		
		System.out.println("\nDirected Graph…..\n  Adjacency Matrix :-\n---------------");
		System.out.print("    ");
		for (int i = 0; i < numberOfVertex; i++)
			System.out.print(i + " ");
		System.out.println("\n   ------------");
		for (int i = 0; i < numberOfVertex; i++) {
			System.out.print(i + " | ");
			for (int j = 0; j < numberOfVertex; j++)
				System.out.print(matrixForDirected[i][j] + " ");
			System.out.println();
		}
		
		System.out.println("\nAdjacency List: -\n--------------");
		for (int i = 0; i < numberOfVertex; i++)
			System.out.println(i + " --> " + adjList[i]);
		
		System.out.println("\nOut degree: -\n-----------");
		for (int i = 0; i < numberOfVertex; i++)
			System.out.println(i + " --> " + totalDegree[i]);
		
		System.out.println("\nOut/IN degree: -\n-------------");
		for (int i = 0; i < numberOfVertex; i++)
			System.out.println(i + " --> " + outDegree[i] + "/" + inDegree[i]);
	}
}

/*
OUTPUT
------
------
Undirected Graph…..
Adjacency Matrix :-
----------------
    0 1 2 3 4 5 
 ------------
0 | 0 1 0 1 0 1 
1 | 1 0 0 0 1 1 
2 | 0 0 0 0 1 0 
3 | 1 0 0 0 1 0 
4 | 0 1 1 1 0 0 
5 | 1 1 0 0 0 0 

Directed Graph…..
Adjacency Matrix :-
---------------
    0 1 2 3 4 5 
 ------------
0 | 0 1 0 1 0 1 
1 | 0 0 0 0 1 1 
2 | 0 0 0 0 1 0 
3 | 0 0 0 0 1 0 
4 | 0 0 0 0 0 0 
5 | 0 0 0 0 0 0 

Adjacency List: -
--------------
0 --> [3, 1, 5]
1 --> [0, 4, 5]
2 --> [4]
3 --> [0, 4]
4 --> [1, 2, 3]
5 --> [0, 1]

Out degree: -
-----------
0 --> 3
1 --> 3
2 --> 1
3 --> 2
4 --> 3
5 --> 2

Out/IN degree: -
-------------
0 --> 3/0
1 --> 2/1
2 --> 1/0
3 --> 1/1
4 --> 0/3
5 --> 0/2
*/