package lab01_17101537;

/**
* @author  Amirul Islam Al Mamun
* @id      17101537
* @date    Sep 22, 2018
* 
* @task read a input file first "input.txt"
**/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LAB01_Task02 {
	public static void main(String[] args) {
		ArrayList <String> inputLine = new ArrayList<String>();
		try (BufferedReader r = new BufferedReader(new FileReader("/home/mamun/workspace/CSE220/CSE221/src/lab01_17101537/input.txt"))) {
			String line;
			while ((line = r.readLine()) != null) {
				inputLine.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Printing File:\n----------");
		for (int i = 0; i < inputLine.size(); i++)
			System.out.println(inputLine.get(i));
	}
}

/*
OUTPUT:
------
------
Printing File:
----------
6
0 3
0 1
0 5
1 4
1 5
2 4
3 4
*/
