package lab02_17101537;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Sep 27, 2018
 **/


import java.util.Scanner;
import java.util.Arrays;

public class SortTest {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);

        int theArray[] = {3, 5 , 10, 23, 25, 8, 7, 9, 50, 47};
        System.out.println("Original Array:\n" + Arrays.toString(theArray));
        
        System.out.println("\nEnter 1 to insertion sort:\nEnter 2 to merge sort:\nEnter 3 to quick sort:\nEnter any value to default sort:\n");
        
        while (sc.hasNext()) {
            int enter = sc.nextInt();
            
            MyArray m = new MyArray(theArray);
            
            if (enter == 1) {
                int insertionSortedArray[] = m.insertionSort();
                System.out.println("insertion sort implemetation:\n" + Arrays.toString(insertionSortedArray));
                System.out.println("Time: " + System.currentTimeMillis() + " \n");
            }
            
            else if (enter == 2) {
                int mergeSortedArray[] = m.mergeSort();
                System.out.println("merge sort implemetation:\n" + Arrays.toString(mergeSortedArray));
                System.out.println("Time: " + System.currentTimeMillis() + " \n");
            }
            
            else if (enter == 3) {
                int quickSortedArray[] = m.quickSort(0, theArray.length-1);
                System.out.println("quick sort implemetation:\n" + Arrays.toString(quickSortedArray));
                System.out.println("Time: " + System.currentTimeMillis() + " \n");
            }
            
            else if (enter == 4) {
                int len = theArray.length;
                int heapSortedArray[] = m.heap_sort(len);
                System.out.println("heap sort implemetation:\n" + Arrays.toString(heapSortedArray));
                System.out.println("Time: " + System.currentTimeMillis() + " \n");
            }
            
            else {
                Arrays.sort(theArray);
                System.out.println("default sort implementation:\n" + Arrays.toString(theArray));
                System.out.println("Time: " + System.currentTimeMillis() + "\n");
            }
        }
    }
}

/*

OUTPUT
------
Original Array:
[3, 5, 10, 23, 25, 8, 7, 9, 50, 47]

Enter 1 to insertion sort:
Enter 2 to merge sort:
Enter 3 to quick sort:
Enter any value to default sort:

1
insertion sort implemetation:
[3, 5, 7, 8, 9, 10, 23, 25, 47, 50]
Time: 1538246775741 

2
merge sort implemetation:
[3, 5, 7, 8, 9, 10, 23, 25, 47, 50]
Time: 1538246777063 

3
quick sort implemetation:
[3, 5, 7, 8, 9, 10, 23, 25, 47, 50]
Time: 1538246777528 

4
default sort implementation:
[3, 5, 7, 8, 9, 10, 23, 25, 47, 50]
Time: 1538246778030

5
default sort implementation:
[3, 5, 7, 8, 9, 10, 23, 25, 47, 50]
Time: 1538246778480

*/