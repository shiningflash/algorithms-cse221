package lab02_17101537;

/**
 * @author  Amirul Islam
 * @id      17101537
 * @date    Sep 27, 2018
 **/

import java.util.Arrays;

public class MyArray {
    int readArray[];
    int size;

    public MyArray(int passedArray[]) {
        readArray = passedArray;
        size = passedArray.length;
    }

    int[] insertionSort() {
        for (int i = 1; i < size; i++) {
            int j = i, temp = readArray[i];
            while (j > 0 && readArray[j-1] > temp) {
                readArray[j] = readArray[j-1];
                j--;
            }
            readArray[j] = temp;
        }
        return readArray;
    }

    int[] mergeSort() {
        mergeSortOperation(0, size-1);
        return readArray;
    }

    void mergeSortOperation(int low, int high) {
        if (low == high) return;
        int mid = (low + high) / 2;

        mergeSortOperation(low, mid);
        mergeSortOperation(mid+1, high);
        mergeTheArray(low, mid, high);
    }

    void mergeTheArray(int low, int mid, int high) {
        int temp[] = new int[size];
        int i = low, j = mid + 1, k = 0;
        while (i <= mid && j <= high) {
            if (readArray[i] < readArray[j]) temp[k++] = readArray[i++];
            else temp[k++] = readArray[j++];
        }
        while (i <= mid) {
            temp[k++] = readArray[i++];
        }
        while (j <= high) {
            temp[k++] = readArray[j++];
        }
    }

    int[] quickSort(int low, int high) {
        if (low < high) {
            int pivotIndex = partitionForQuickSort(low, high);
            quickSort(low, pivotIndex - 1);
            quickSort(pivotIndex + 1, high);
        }
        return readArray;
    }

    int partitionForQuickSort(int low, int high) {
        int pivot = readArray[high];
        int  i = low - 1;
        for (int j = low; j < high; j++) {
            if (readArray[j] <= pivot) {
                i++;
                swap(readArray[i], readArray[j]);
            }
        }
        swap(readArray[i+1], readArray[high]);
        return i+1;
    }

    void swap(int a, int b) {
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
    }
    
    int left(int i) { return 2 * i + 1; }
    int right(int i) { return 2 * i + 2; }
    int parent(int i) { return i / 2; }

    void max_heapify(int heap_size, int i) {
        int l = left(i);
        int r = right(i);
        int largest = i;
        if(l < heap_size && readArray[l] > readArray[largest]) largest = l;
        if(r < heap_size && readArray[r] > readArray[largest]) largest = r;
        if(largest != i) {
//            swap(readArray[i], readArray[largest]);
            int temp = readArray[i];
            readArray[i] = readArray[largest];
            readArray[largest] = temp;
            max_heapify(heap_size, largest);
        }
    }

    int[] heap_sort(int len) {
        size = len;
        for(int i = len / 2 - 1; i >= 0; i--) max_heapify(size, i);
        for (int i = size-1; i >= 0; i--) {
//            swap(readArray[0], readArray[i]);
            int temp = readArray[0];
            readArray[0] = readArray[i];
            readArray[i] = temp;
            max_heapify(i, 0);
        }
        return readArray;
    }

    public String toString() {
        return Arrays.toString(readArray);
    }
}