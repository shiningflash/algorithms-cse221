package lab03_17101537;

/***********************************************
 * @author Amirul Islam Al Mamun (17101537)
 * @date   09 October, 2018
 *  
 * @task
 * First line contains Test Case, takes from the user
 * for each Test Case, takes two space-separated integers (numberOfVertex, numberOfEdges)
 * the next numberOfEdges lines takes two space-separated integers donated the bridge between theVertex & connectedVertex
 * 
 *  @work
 *  after forming Undirected Adj. Matrix, traverse the Graph by BFS algorithm
 *  and then prints the minimum distance from Node #1 to Node #N 
 ************************************************/

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Task03 {
	
	protected static int numberOfVertex;
	protected static int numberOfEdges;
	protected static int matrixUndirected[][];
	protected static Integer distance[];
	protected static color colors[];
	
	protected static enum color { WHITE, GRAY, BLACK; }
	
	protected static void MatrixFormingByTakingInput() {
		Scanner sc = new Scanner(System.in);
		numberOfVertex = sc.nextInt();
		numberOfEdges = sc.nextInt();
		
		matrixUndirected = new int[numberOfVertex+1][numberOfVertex+1];
		
		for (int i = 1; i <= numberOfEdges; i++) {
			int theVertex = sc.nextInt();
			int connectedVertex = sc.nextInt();
			
			matrixUndirected[theVertex][connectedVertex] = 1;
			matrixUndirected[connectedVertex][theVertex] = 1;
		}
	}
	
	protected static void Initialization() {
		colors = new color[numberOfVertex+1];
		distance = new Integer[numberOfVertex+1];
	}
	
	protected static void BFS(int source) {
		for (int i = 1; i <= numberOfVertex; i++) {
			colors[i] = color.WHITE;
			distance[i] = Integer.MAX_VALUE;
		}
		colors[source] = color.GRAY;
		distance[source] = 0;
		Queue <Integer> q = new LinkedList<Integer>();
		q.add(source);
		while (!q.isEmpty()) {
			int u = q.poll();
			for (int v = 1; v <= numberOfVertex; v++) {
				if (matrixUndirected[u][v] == 1 && colors[v] == color.WHITE) {
					colors[v] = color.GRAY;
					distance[v] = distance[u] + 1;
					q.add(v);
				}
			}
			colors[u] = color.BLACK;
		}
	}
	
	protected static void PrintMinimumNumberOfBridges() {
		System.out.println(distance[numberOfVertex]);
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCase = sc.nextInt();
		for (int i = 1; i <= testCase; i++) {
			MatrixFormingByTakingInput();
			Initialization();
			BFS(1);   // source node --> 1 
			PrintMinimumNumberOfBridges(); // print minimum number of bridges that have to cross
		}
	}
}

/*************************************************
 ** INPUT ****************************************
    2 (#numberOfTestCase)

    3 2 (#numberOfVertex) (numberOfEdges)
    1 2
    2 3

    4 4 (#numberOfVertex) (numberOfEdges)
    1 2
    2 3
    3 4
    2 4
 **************************************************
 ** OUTPUT ****************************************
    2 (for test case 1)
    2 (for test case 2)
 ************************************************/