package lab03_17101537;

/***********************************************
 * @author Amirul Islam Al Mamun (17101537)
 * @date   09 October, 2018
 *  
 * reads a Graph.txt file from the desktop
 * creates a Adj. Matrix using the text file
 * traverse the Graph BFS algorithm
 * prints the optimal distance and shortest path for each node from the source node 
 ************************************************/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Queue;

public class Task02 {
	
	protected static int numberOfVertex;
	protected static int numberOfEdges;
	protected static int matrixUndirected[][];
	protected static ArrayList<Integer> shortest_path[];
	protected static Integer predecessor[];
	protected static Integer distance[];
	protected static color colors[];
	
	protected static enum color { WHITE, GRAY, BLACK; }
	
	protected static void FileReaderMethod() {
		ArrayList<String> inputLine = new ArrayList<String>();
		try {
			BufferedReader r = new BufferedReader(new FileReader("/home/mamun/workspace/CSE220/CSE221/src/lab03_17101537/Graph.txt"));
			String line;
			while((line = r.readLine()) != null) {
				inputLine.add(line);
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		numberOfVertex = Integer.parseInt(inputLine.get(0));
		numberOfEdges = Integer.parseInt(inputLine.get(1));
		
		matrixUndirected = new int[numberOfVertex+1][numberOfVertex+1];
		
		for (int i = 1, k = 2; i <= numberOfEdges * 2; i++, k++) {
			String temp[] = inputLine.get(k).split(" ", 2);
			int theVertex = Integer.parseInt(temp[0]);
			int connectedVertex = Integer.parseInt(temp[1]);
			
			matrixUndirected[theVertex][connectedVertex] = 1;
		}
	}
	
	protected static void PrintAdjMatrix() {
		System.out.println("Undirected Graph..\n  Adjacency Matrix :-\n----------------");
		System.out.print("    ");
		for (int i = 1; i <= numberOfVertex; i++) System.out.print(i + " ");
		System.out.println("\n   ------------");
		for (int i = 1; i <= numberOfVertex; i++) {
			System.out.print(i + " | ");
			for (int j = 1; j <= numberOfVertex; j++)
				System.out.print(matrixUndirected[i][j] + " ");
			System.out.println();
		}
		System.out.println();
	}
	
	protected static void Initialization() {
		colors = new color[numberOfVertex+1];
		distance = new Integer[numberOfVertex+1];
		shortest_path = new ArrayList[numberOfVertex+1];
		predecessor = new Integer[numberOfVertex+1];
		for (int i = 1; i <= numberOfVertex; i++)
			shortest_path[i] = new ArrayList<Integer>();
	}
	
	protected static void BFS(int source) {
		for (int i = 1; i <= numberOfVertex; i++) {
			colors[i] = color.WHITE;
			distance[i] = Integer.MAX_VALUE;
			predecessor[i] = null;
		}
		colors[source] = color.GRAY;
		distance[source] = 0;
		predecessor[source] = null;
		Queue <Integer> q = new LinkedList<Integer>();
		q.add(source);
		while (!q.isEmpty()) {
			int u = q.poll();
			for (int v = 1; v <= numberOfVertex; v++) {
				if (matrixUndirected[u][v] == 1 && colors[v] == color.WHITE) {
					colors[v] = color.GRAY;
					distance[v] = distance[u] + 1;
					predecessor[v] = u; 
					q.add(v);
				}
			}
			colors[u] = color.BLACK;
		}
	}
	
	protected static void ShortestPATH() {
		for (int k = 1; k <= numberOfVertex; k++) {
			Integer p = k; shortest_path[k].add(k);
			while(predecessor[p] != null) {
				shortest_path[k].add(predecessor[p]);
				p = predecessor[p];
			}
		}
	}
	
	protected static void PrintPATH() {
		System.out.println("node: distance from source ----- path --------\n----------------------------------------------");
		for (int i = 1; i <= numberOfVertex; i++) {
			System.out.print("  " + i  +  " : distance from source - " + distance[i] + ",  path -> ");
			for (int k = shortest_path[i].size()-1; k >= 0; k--)
				System.out.print((k == 0) ? shortest_path[i].get(k) : shortest_path[i].get(k) + ", ");
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		FileReaderMethod();
		PrintAdjMatrix();
		Initialization();
		BFS(1);  // 1 -> source node 
		ShortestPATH();
		PrintPATH();
	}
}

/**************************************************
 ** INPUT: ********** Graph.txt *******************
 
    6 (#numberOfVertex)
    8 (#numberOfEdges)
    1 2
    1 3
    2 1
    2 4
    2 5
    3 1
    3 5
    4 2
    4 5
    4 6
    5 2
    5 3
    5 4
    5 6
    6 4
    6 5
 *************************************************
 ** OUTPUT: ***************************************

 Undirected Graph..
  Adjacency Matrix :-
----------------
    1 2 3 4 5 6 
   ------------
1 | 0 1 1 0 0 0 
2 | 1 0 0 1 1 0 
3 | 1 0 0 0 1 0 
4 | 0 1 0 0 1 1 
5 | 0 1 1 1 0 1 
6 | 0 0 0 1 1 0 

node: distance from source ----- path --------
----------------------------------------------
  1 : distance from source - 0,  path -> 1
  2 : distance from source - 1,  path -> 1, 2
  3 : distance from source - 1,  path -> 1, 3
  4 : distance from source - 2,  path -> 1, 2, 4
  5 : distance from source - 2,  path -> 1, 2, 5
  6 : distance from source - 3,  path -> 1, 2, 4, 6
***************************************************/