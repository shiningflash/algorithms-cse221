package lab03_17101537;

/***********************************************
 * @author Amirul Islam Al Mamun (17101537)
 * @date   09 October, 2018
 *  
 * @work
 * First line contains two space-separated integers (numberOfVertex, numberOfEdges), taken from the user
 * Runs BFS algorithm to the newly formed Graph
 * prints the path
 ************************************************/

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

// BFS (Breadth First Search)

public class Task01 {
	
	protected static int numberOfVertex, numberOfEdges;
	protected static int matrixUndirected[][];
	protected static Integer predecessor[], distance[]; 
	protected static color colors[];
	
	protected static enum color { WHITE, GRAY, BLACK; }
	
	protected static void Initialization() {
		colors = new color[numberOfVertex+1];
		distance = new Integer[numberOfVertex+1];
		predecessor = new Integer[numberOfVertex+1];
	}
	
	protected static void MatrixFormingByTakingInput() {
		Scanner sc = new Scanner(System.in);
		numberOfVertex = sc.nextInt(); numberOfEdges = sc.nextInt();
		
		matrixUndirected = new int[numberOfVertex+1][numberOfVertex+1];
		
		for (int i = 1; i <= numberOfEdges; i++) {
			int theVertex = sc.nextInt();
			int connectedVertex = sc.nextInt();
			
			matrixUndirected[theVertex][connectedVertex] = 1;
			matrixUndirected[connectedVertex][theVertex] = 1;
		}
	}
	
	protected static void BFS(int source) {
		for (int i = 1; i <= numberOfVertex; i++) {
			colors[i] = color.WHITE;
			distance[i] = Integer.MAX_VALUE;
			predecessor[i] = null;
		}
		colors[source] = color.GRAY;
		distance[source] = 0;
		predecessor[source] = null;
		Queue <Integer> q = new LinkedList<Integer>();
		q.add(source);
		while (!q.isEmpty()) {
			int u = q.poll();
			System.out.println(u);
			for (int v = 1; v <= numberOfVertex; v++) {
				if (matrixUndirected[u][v] == 1 && colors[v] == color.WHITE) {
					colors[v] = color.GRAY;
					distance[v] = distance[u] + 1;
					predecessor[v] = u; 
					q.add(v);
				}
			}
			colors[u] = color.BLACK;
		}
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		MatrixFormingByTakingInput();
		Initialization();
		BFS(1);   // source node --> 1 
	}
}

/***********************************************
 INPUT:
 4 4 (#numberOfVertex) (#numberOfEdges)
 1 4
 4 2
 2 3
 3 4
 
 OUTPUT: (BFS traverse)
 1
 4
 2
 3
 ************************************************/