package lab03_practice;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Queue;

public class BFS_program1 {
	
	protected static int numberOfVertex;
	protected static int numberOfEdges;
	protected static int matrixUndirected[][];
	protected static ArrayList<Integer> shortest_path[];
	protected static Integer predecessor[];
	protected static Integer distance[];
	protected static color colors[];
	
	protected static enum color { WHITE, GRAY, BLACK; }
	
	protected static void FileReaderMethod() {
		ArrayList<String> inputLine = new ArrayList<String>();
		try {
			BufferedReader r = new BufferedReader(new FileReader("/home/mamun/workspace/CSE220/CSE221/src/lab03_practice/input.txt"));
			String line;
			while((line = r.readLine()) != null) {
				inputLine.add(line);
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		numberOfVertex = Integer.parseInt(inputLine.get(0));
		numberOfEdges = Integer.parseInt(inputLine.get(1));
		
		matrixUndirected = new int[numberOfVertex+1][numberOfVertex+1];
		
		for (int i = 1, k = 2; i <= numberOfEdges * 2; i++, k++) {
			String temp[] = inputLine.get(k).split(" ", 2);
			int theVertex = Integer.parseInt(temp[0]);
			int connectedVertex = Integer.parseInt(temp[1]);
			
			matrixUndirected[theVertex][connectedVertex] = 1;
		}
	}
	
	protected static void PrintAdjMatrix() {
		System.out.println("Undirected Graph…..\n  Adjacency Matrix :-\n----------------");
		System.out.print("    ");
		for (int i = 1; i <= numberOfVertex; i++) System.out.print(i + " ");
		System.out.println("\n   ------------");
		for (int i = 1; i <= numberOfVertex; i++) {
			System.out.print(i + " | ");
			for (int j = 1; j <= numberOfVertex; j++)
				System.out.print(matrixUndirected[i][j] + " ");
			System.out.println();
		}
		System.out.println();
	}
	
	protected static void Initialization() {
		colors = new color[numberOfVertex+1];
		distance = new Integer[numberOfVertex+1];
		shortest_path = new ArrayList[numberOfVertex+1];
		predecessor = new Integer[numberOfVertex+1];
		for (int i = 1; i <= numberOfVertex; i++)
			shortest_path[i] = new ArrayList<Integer>();
	}
	
	protected static void BFS(int source) {
		for (int i = 1; i <= numberOfVertex; i++) {
			colors[i] = color.WHITE;
			distance[i] = Integer.MAX_VALUE;
			predecessor[i] = null;
		}
		colors[source] = color.GRAY;
		distance[source] = 0;
		predecessor[source] = null;
		Queue <Integer> q = new LinkedList<Integer>();
		q.add(source);
		while (!q.isEmpty()) {
			int u = q.poll();
			for (int v = 1; v <= numberOfVertex; v++) {
				if (matrixUndirected[u][v] == 1 && colors[v] == color.WHITE) {
					colors[v] = color.GRAY;
					distance[v] = distance[u] + 1;
					predecessor[v] = u; 
					q.add(v);
				}
			}
			colors[u] = color.BLACK;
		}
	}
	
	protected static void ShortestPATH() {
		for (int k = 1; k <= numberOfVertex; k++) {
			Integer p = k; shortest_path[k].add(k);
			while(predecessor[p] != null) {
				shortest_path[k].add(predecessor[p]);
				p = predecessor[p];
			}
		}
	}
	
	protected static void PrintPATH() {
		System.out.println("node: distance from source ----- path --------\n----------------------------------------------");
		for (int i = 1; i <= numberOfVertex; i++) {
			System.out.print("  " + i  +  " : distance from source - " + distance[i] + ",  path -> ");
			for (int k = shortest_path[i].size()-1; k >= 0; k--)
				System.out.print((k == 0) ? shortest_path[i].get(k) : shortest_path[i].get(k) + ", ");
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		FileReaderMethod();
		PrintAdjMatrix();
		Initialization();
		BFS(1);   // source node --> 1 
		ShortestPATH();
		PrintPATH();
	}
}
